#!/bin/bash
set -xeo pipefail

source lib/aws.sh

echo "[+] Install dependencies"
install_dependencies

echo "[+] Configure AWS settings"
configure_aws_settings "${AWS_REGION}"

BUILD_FORMAT=$(set_build_format)

IMAGE_FILE="${IMAGE_KEY}.${BUILD_FORMAT}"
if [ ! -r "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" ]; then
  echo "Error: the file ${IMAGE_FILE} doesn't exist"
  exit 1
fi
IMAGE_FILE=$(compress_image)

create_checksum_file $DOWNLOAD_DIRECTORY $IMAGE_FILE

S3_UPLOAD_PREFIX=$(set_s3_upload_prefix)
echo "[+] Uploading raw image to 's3://${S3_BUCKET_NAME}/${S3_KEY}'"
s3_cp "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"
s3_cp "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}.sha256" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"

if [[ "${TEST_IMAGE}" == "yes" ]] &&
   [[ "${IMAGE_NAME}" == @(qa|qaqcom) ]] && [[ "${IMAGE_TYPE}" == "ostree" ]] ; then
   UPGRADE_REPO_DIR="${DOWNLOAD_DIRECTORY}/repo/${IMAGE_KEY}.repo"
   echo "[+] Creating index.html ${UPGRADE_REPO_DIR}"
   tree -H . "${UPGRADE_REPO_DIR}" -o "${UPGRADE_REPO_DIR}"/index.html
   echo "[+] Uploading repo 's3://${S3_BUCKET_NAME}/${S3_KEY}'"
   s3_cp "${UPGRADE_REPO_DIR}" \
         "${S3_BUCKET_NAME}/repo/${IMAGE_KEY}.repo" \
         "--recursive"
fi

# Only import images used for testing in the pipeline
if do_not_import_image ; then
  echo "[+] The image is not for testing, it won't be imported as AMI"
  exit 0
fi

echo "[+] Import snapshot to EC2"

S3_KEY="${S3_UPLOAD_PREFIX}/${IMAGE_FILE}"
IMPORT_SNAPSHOT_ID=$(import_snapshot "${S3_BUCKET_NAME}" "${S3_KEY}")

echo "[+] Waiting for snapshot $IMPORT_SNAPSHOT_ID import"
status=$(task_progress import_status "$IMPORT_SNAPSHOT_ID" "completed" "deleted")
echo "snapshot status: $status"

describe_import_snapshot_tasks "$IMPORT_SNAPSHOT_ID"
SNAPSHOT_ID=$(snapshot_id "$IMPORT_SNAPSHOT_ID")
create_snapshot_tags "$SNAPSHOT_ID"

echo "[+] Removing RAW images and json files from S3 bucket"
delete_raw_images "${S3_BUCKET_NAME}" "${S3_UPLOAD_PREFIX}" "${IMAGE_KEY}"

# SHARING AMI
if [[ "${IMAGE_NAME}" == "cki" ]]; then
  aws_target_region="${AWS_CKI_REGION}"
  aws_target_account_id="${AWS_CKI_ACCOUNT_ID}"
else
  aws_target_region="${AWS_TF_REGION}"
  aws_target_account_id="${AWS_TF_ACCOUNT_ID}"
fi
echo "[+] Copy snapshot from ${AWS_REGION} to ${aws_target_region}"
SNAPSHOT_ID=$(copy_snapshot "$SNAPSHOT_ID" "${AWS_REGION}" "${aws_target_region}")

echo "[+] Waiting for copy to complete"
status=""
status=$(task_progress snapshot_state "$SNAPSHOT_ID:${aws_target_region}" "completed")
echo "snapshot status: $status"

echo "[+] Register AMI from snapshot"
arch=$(fix_arch_name "${ARCH}")
root_device="/dev/sda1"
boot_mode="uefi"
IMAGE_ID=$(register_image "${IMAGE_KEY}" \
                          "${aws_target_region}" \
                          "${arch}" \
                          "${root_device}" \
                          "${boot_mode}" \
                          "$SNAPSHOT_ID" \
          )

create_image_tags "$IMAGE_ID" \
                  "${aws_target_region}" \
                  "${UUID}"

# Wait for image registration
echo "[+] Waiting for image $IMAGE_ID registration"
status=$(task_progress image_register_status "${IMAGE_ID}:${aws_target_region}" "available")
echo "image registration status: $status"

# Give permissions to the Testing Farm provisioner account
echo "[+] Granting permissions for $IMAGE_ID to the Testing Farm provisioner"
grant_image_permissions "${IMAGE_ID}" "${aws_target_region}" "${aws_target_account_id}"

echo "Image info:"
image_register_info "$IMAGE_ID" "${aws_target_region}"

# We want to publish the AMI name when IMPORT_IMAGE is set, so the QE teams can use it
# in Testing Farm
if [[ "${IMPORT_IMAGE}" =~ ^(True|yes)$ ]]; then
  echo "[+] Publish AMI info"
  publish_ami_info "$IMAGE_ID"
fi
