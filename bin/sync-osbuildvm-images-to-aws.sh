#!/bin/bash
set -xeo pipefail

source lib/aws.sh

echo "[+] Install dependencies"
install_dependencies

echo "[+] Configure AWS settings"
configure_aws_settings "${AWS_REGION}"

S3_UPLOAD_PREFIX=$(set_s3_upload_prefix)
echo "[+] Uploading osbuildvm-images to 's3://${S3_BUCKET_NAME}/${S3_KEY}'"
s3_cp "${DOWNLOAD_DIRECTORY}/osbuildvm-${ARCH}.img" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"
s3_cp "${DOWNLOAD_DIRECTORY}/osbuildvm-${ARCH}.initramfs" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"
s3_cp "${DOWNLOAD_DIRECTORY}/osbuildvm-${ARCH}.vmlinuz" \
      "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"
