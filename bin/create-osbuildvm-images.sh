#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh

# The sample-images makefile has two types of targets:
# 1) build osbuildvm-images which takes no configuration
# 2) build sample-images which can be highly configured
# The script handles the osbuildvm-images case and applies completely different
# behaviour than the other case since it's not very similar to the other
# (different artefacts, different configuration, etc).
# The other case is handled in create-osbuild.sh.

set_vars
install_dependencies
cd "${MANIFEST_DIR}"
build_target osbuildvm-images

echo "[+] Moving the generated image files"
mkdir -p "${DOWNLOAD_DIRECTORY}"
mv "_build/osbuildvm-${ARCH}.img" "${DOWNLOAD_DIRECTORY}"
mv "_build/osbuildvm-${ARCH}.initramfs" "${DOWNLOAD_DIRECTORY}"
mv "_build/osbuildvm-${ARCH}.vmlinuz" "${DOWNLOAD_DIRECTORY}"

cleanup
